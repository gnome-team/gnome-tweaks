#!/bin/sh

# GNOME Tweaks no longer provides an on/off switch
# for the lid inhibitor
rm .config/autostart/ignore-lid-switch-tweak.desktop
